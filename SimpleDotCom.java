import java.util.Arrays;

public class SimpleDotCom {
    int [] locationCells;
    int numOfHits = 0;

    String checkYourself(String userGuess) {
        try{
            int guess = Integer.parseInt(userGuess);
            String result = "miss";
            int index=0;

            for (int cell : locationCells) {
                index++;
                if (guess == cell) {
                    for(int i=0;i<locationCells.length;i++){
                        if(locationCells[i]==cell) locationCells[i]=-1;
                    }
                    result = "hit";
                    numOfHits++;
                    break;
                }
            }
            if (numOfHits == locationCells.length) {
                result = "kill";
            }
            System.out.println(result);
            return result;
        }catch (NumberFormatException e){
            System.out.println("Invalid Input");
        }
        return "Input Again";
    }

    void setLocationsCells(int[] cellLocations){
        locationCells = cellLocations;
    }
}
