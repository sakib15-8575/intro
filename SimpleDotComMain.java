import java.util.Scanner;

public class SimpleDotComMain {
    public static void main(String[] args) {
        boolean isAlive = true;
        int numOfGuesses = 0;
        int random = (int) (Math.random()*5);
        SimpleDotCom dot = new SimpleDotCom();

        int[] locations = {random, random+1, random+2};
        dot.setLocationsCells(locations);

        Scanner scan = new Scanner(System.in);
        while (isAlive==true) {
            System.out.print("Enter a number ");
            String userGuess = scan.next();
            String result = dot.checkYourself(userGuess);
            numOfGuesses++;
            if(result.equals("kill")){
                isAlive = false;
                System.out.println("You took "+numOfGuesses+" guesses!!");
            }
        }
    }
}
